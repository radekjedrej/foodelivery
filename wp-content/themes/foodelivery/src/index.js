// import './js/navigation';
// import './js/skip-link-focus-fix';

import $ from 'jquery'

import { Locomotive } from "./js/locomotive"
import { SwiperCarousel } from "./js/swiper"
import { Pricing } from "./js/pricing"
// import { Cursor } from "./js/cursor"
import Login from "./js/login";
import { vh } from './js/helpers'
import { Accordion } from "./js/faq";
import { Magnet } from "./js/magnet";

window.addEventListener("DOMContentLoaded", () => {

    new Locomotive();
    new SwiperCarousel();
    new Pricing();
    new Magnet();
    new Login();


    // const cursor = new Cursor()
    
    Accordion();
    vh();


    const cursor = document.querySelector('.u-pros__video');
    
    if(cursor) {
      const tastyMenu = document.querySelectorAll('.tasty-menu__content')
      const link = document.querySelectorAll('.hover-this');
      const linkInner = document.querySelectorAll('.hover-this__inner');
      const cursorContent = document.querySelector('.u-pros__video__content');
      let timeout;
  
      const animateit = function (e) {
        const span = this.querySelector('span');
  
          const { offsetX: x, offsetY: y } = e,
          { offsetWidth: width, offsetHeight: height } = this,
  
          move = 25,
          xMove = x / width * (move * 2) - move,
          yMove = y / height * (move * 2) - move;
  
          span.style.transform = `translate(${xMove}px, ${yMove}px)`;
  
          if (e.type === 'mouseleave') {
            span.style.transform = '';
            cursor.classList.remove('u-pros__video--enter')
          } 
      };
  
      const editCursor = e => {
        const { clientX: x, clientY: y } = e;
        cursor.style.left = x + 'px';
        cursor.style.top = y + 'px';
        
        const { offsetY: oy } = e
        const { offsetX: ox } = e
  
        cursor.style.transform = (`scale(1.03${oy}, 0.96${oy}) translate(-50%, -50%)`);
  
        clearTimeout(timeout);
        timeout = setTimeout(function(){
          cursor.style.transform = ("scale(1, 1) translate(-50%, -50%)");
        }, 20);
      };
  
      const addvideo = function (e) {
  
        console.log('inside');
        const videoSrc = this.getAttribute("data-video");
        cursorContent.src = videoSrc
        cursor.classList.add('u-pros__video--enter')
      }
  
      const increaseSize = e => {
        cursor.classList.add('u-pros__video--increase')
      };
  
      const decreaseSizeSize = e => {
        cursor.classList.remove('u-pros__video--increase')
      };
  
      const tastyMenuEnter = e => {
        cursor.classList.add('u-pros__composition')
      }
  
      const tastyMenuLeave = e => {
        cursor.classList.remove('u-pros__composition')
      }
  
      link.forEach(b => b.addEventListener('mousemove', animateit));
      link.forEach(b => b.addEventListener('mouseenter', addvideo));
      linkInner.forEach(b => b.addEventListener('mouseenter', increaseSize));
      linkInner.forEach(b => b.addEventListener('mouseleave', decreaseSizeSize));
      link.forEach(b => b.addEventListener('mouseleave', animateit));
  
      tastyMenu.forEach(b => b.addEventListener('mouseenter', tastyMenuEnter));
      tastyMenu.forEach(b => b.addEventListener('mouseleave', tastyMenuLeave));
  
      window.addEventListener('mousemove', editCursor);
    }




  //  Popup
  //open popup
  $('.cd-popup-trigger').on('click', function(event){
    event.preventDefault();
    $('.cd-popup').addClass('is-visible');
  });
  
  //close popup
  $('.cd-popup').on('click', function(event){
    if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
      event.preventDefault();
      $(this).removeClass('is-visible');
    }
  });
  //close popup when clicking the esc keyboard button
  $(document).on('keyup', function(event){
      if(event.which=='27'){
        $('.cd-popup').removeClass('is-visible');
      }
  });

})
