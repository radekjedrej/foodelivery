import Swiper, { Navigation, Autoplay } from 'swiper'  
import 'swiper/swiper.scss'

export class SwiperCarousel {
  constructor() {
    Swiper.use([Navigation, Autoplay]);

    this.continuous;
    this.testimonialCarousel;
    this.insta;
    this.homeBanner

    this.init()
  }

  init() {
    this.tastyMenu();
    this.contionuousCarousel();
    this.testimonial();
    this.instaCarousel();
  }

  tastyMenu() {
    this.homeBanner = new Swiper(".tasty-menu__container", {
      slidesPerView: 1,
      freeMode: true,
      breakpoints: {
        640: {
          slidesPerView: 2
        },
        1024: {
          slidesPerView: 3,
          navigation: {
            nextEl: '.tasty-menu__next',
            prevEl: '.tasty-menu__prev',
          },
          mousewheel: {
            forceToAxis: true
          }
        }
      }
    });
  }

  contionuousCarousel()  {
    this.continuous = new Swiper(".continious-carousel__container", {
      autoplay: {
        delay: 1,
        disableOnInteraction: false
      },
      loop: true,
      slidesPerView: 'auto',
      speed: 1000,
      grabCursor: false,
      mousewheelControl: true,
      keyboardControl: true,
    });
  }

  testimonial() {
    this.testimonialCarousel = new Swiper('.testimonial__container', {
        loop: true,
        slidesPerView: 1,
        centeredSlides: true,
        spaceBetween: 20,
        navigation: {
          nextEl: '.testimonial__next',
          prevEl: '.testimonial__prev',
        },
        breakpoints: {
            1000: {
                slidesPerView: 2,
                spaceBetween: 120,
            }
        }
    });
  }

  instaCarousel() {
    this.insta = new Swiper('.insta-carousel', {
      slidesPerView: 5,
      spaceBetween: 0,
      freeMode: true,
      navigation: {
        nextEl: '.insta-carousel__next',
        prevEl: '.insta-carousel__prev',
      },
    });
  }
}