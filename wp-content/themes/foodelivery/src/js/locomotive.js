import LocomotiveScroll from 'locomotive-scroll';

export class Locomotive {
    constructor() {
        this.scroll

        this.init()
    }

    init() {
        this.scroll = new LocomotiveScroll({
            el: document.querySelector('[data-scroll-container]'),
            smooth: true
        });

        setTimeout(() => {
            this.scroll.on('call', (value, way, obj) => {

            const { el } = obj
        
              if (way === 'enter') {
                switch (value) {
                    case "tastybg":
                    const bgtasty = el.getAttribute('data-bg')
                    document.querySelector('.tasty-bg').style.backgroundColor = bgtasty;
                    break;     

                    case "testimonialbg":
                    const bgtestimonial = el.getAttribute('data-bg')
                    document.querySelector('.testimonial-bg').style.backgroundColor = bgtestimonial;
                    break;     
                    
                    case "headingbg":
                    const bgheading = el.getAttribute('data-bg')
                    document.querySelector('.heading-bg').style.backgroundColor = bgheading;
                    break;   
                }
            } 
            });
        }, 800);
    }
}
