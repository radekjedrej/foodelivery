// Handles auto-height transition
export const handleHeight = ( btn ) => {
    const el = btn.nextElementSibling
    const height = el.scrollHeight

    requestAnimationFrame(() => {
        btn.classList.toggle('active')
        el.style.height = !el.style.height
            ? `${height}px`
            : null
    })
}

// Templates :: 
export const Accordion = (() => {

  function toggleAccordion() {
    const button = document.querySelectorAll('.faq__question')

    if(button) {
        for (let i = 0; i < button.length; i++) {
            button[i].addEventListener("click", function() {
                /* Toggle between hiding and showing the active panel */
                handleHeight( this )
            });
        } 
    }
}

toggleAccordion() // Call listener function at run time
    
})
