export class Cursor {

    constructor() {
      this.bind()
  
      this.cursor = document.querySelector('.cursor')
      this.mouseCurrent = {
        x: 0,
        y: 0
      }
      
      this.mouseLast = {
        x: this.mouseCurrent.x,
        y: this.mouseCurrent.y
      }
      
      this.rAF = undefined
    }
    
    bind() {
      ['getMousePosition', 'run'].forEach((fn) => this[fn] = this[fn].bind(this))
    }
    
    getMousePosition(e) {
      this.mouseCurrent = {
        x: e.clientX,
        y: e.clientY
      }
    }
    
    run() {
      this.mouseLast.x = this.lerp(this.mouseLast.x, this.mouseCurrent.x, 0.2)
      this.mouseLast.y = this.lerp(this.mouseLast.y, this.mouseCurrent.y, 0.2)
      
      this.mouseLast.x = Math.floor(this.mouseLast.x * 100) / 100
      this.mouseLast.y = Math.floor(this.mouseLast.y * 100) / 100
      
      this.rAF = requestAnimationFrame(this.run)
    }
    
    requestAnimationFrame() {
      this.rAF = requestAnimationFrame(this.run)
    }
    
    addEvents() {
      window.addEventListener('mousemove', this.getMousePosition, false)
    }
    
    on() {
      this.addEvents()
      this.requestAnimationFrame()
    }
    
    init() {
      this.on()
    }

    lerp(a, b, n) {
        return (1 - n) * a + n * b
    }
    
}