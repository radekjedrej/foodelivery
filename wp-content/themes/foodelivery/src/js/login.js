import $ from 'jquery';

export default class Login {
  constructor() {
    this.loginBtn = $('.login-account #u-login');
    this.registerBtn = $('.login-account #u-register');
    this.loginPage = $('.woocommerce-account .login-account');
    this.showLogin = $('.showlogin');

    this.init();
  }

  init() {
    this.checkLogin();
    this.loginBtn.on('click', this.login);
    this.registerBtn.on('click', this.register);
    this.showLogin.on('click', this.slide);

  }

  login() {
    $('.u-login__tabs__buttons a').removeClass('active');
    // $(this).addClass('active');
    $('.u-register').fadeOut(5);
    $('.u-login').fadeIn(100);
  }

  register() {
    $('.u-login__tabs__buttons a').removeClass('active');
    // $(this).addClass('active');
    $('.u-login').fadeOut(5);
    $('.u-register').fadeIn(100);
  }

  slide() {
    $('.login-account').fadeIn();
    $('.woocommerce-form-login-toggle').fadeOut(5);
  }


  checkLogin() {
    if($('.woocommerce-account .login-account').length > 0) {
      $('body').addClass('login-page');
      $('#afreg_additionalshowhide_3066, #afreg_additionalshowhide_3067').append('<span class="checkmark"></span>');
    }
  }

}
