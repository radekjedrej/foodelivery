export class Pricing {

    constructor() {
        this.tabLinks = document.querySelectorAll(".tablinks");
        this.tabContent = document.querySelectorAll(".tabcontent");
        this.linksWrapper = document.querySelector(".pricing__buttons")
        this.previousDirection = "left"
        this.events()
    }

    events() {
        this.tabLinks.forEach(el => el.addEventListener("click", this.openTabs.bind(this)));
    }
    
    openTabs(el) {
        el.preventDefault()
        const btnTarget = el.currentTarget;
        const country = btnTarget.dataset.country;
        const direction = btnTarget.dataset.direction;

        this.tabContent.forEach((el) => {
            el.classList.remove("active");
        });
        
        this.tabLinks.forEach((el) => {
            el.classList.remove("active");
            el.setAttribute("aria-selected", false)
        });

        // Check for previous direction (if) exist remove class from the button wrapper
        this.previousDirection ? this.linksWrapper.classList.remove(this.previousDirection) : ''
        // Add current direction to the button wrapper
        this.linksWrapper.classList.add(direction)
        // Save current direction 
        this.previousDirection = direction
        // Activate correct tab 
        document.querySelector(`#${country}`).classList.add("active");
        // AActivate correct button
        btnTarget.classList.add("active");
        btnTarget.setAttribute("aria-selected", true)
    }
}