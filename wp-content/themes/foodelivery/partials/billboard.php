<?php

$image = get_sub_field("image") ? get_sub_field("image") : "";
$image_mobile = get_sub_field("image_mobile") ? get_sub_field("image_mobile") : "";
$title = get_sub_field("title") ? get_sub_field("title") : "";
$button = get_sub_field("button") ? get_sub_field("button") : "";

?>

<section class="billboard d-flex d-flex-center" style="background-image: url(<?= $image['url'] ?>)" data-scroll-section>
    <div class="billboard__content d-flex d-flex-column d-flex-center">
        <h1 class="billboard__title heading-billboard color-white u-text-center"><?= $title ?></h1>
        <a class="u-btn" href="<?= $button['url'] ?>"><?= $button['title'] ?></a>
    </div>

    <footer class="trustpilot__wrapper d-flex d-flex-center text-reviews">
        <div class="trustpilot d-flex d-flex-center">
            <img class="trustpilot__logo" src="<?php echo get_theme_file_uri('/src/images/svg/trustpilot-logo.svg'); ?>" alt="trustpilot icon">
            <img class="trustpilot__stars" src="<?php echo get_theme_file_uri('/src/images/svg/trustpilot-filled.svg'); ?>" alt="review stars icon">
            <p>Our customers say Excellent <span class="bold"> 4.8 / 5 based on 2.021 reviews</span></p>
        </div>
    </footer>
</section>