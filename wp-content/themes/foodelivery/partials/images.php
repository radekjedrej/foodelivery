<?php

$image_one = get_sub_field("image_one") ? get_sub_field("image_one") : "";
$image_two = get_sub_field("image_two") ? get_sub_field("image_two") : "";
$image_three = get_sub_field("image_three") ? get_sub_field("image_three") : "";

?>

<div class="f-images" data-scroll-section>
    <div class="wrapper-main">
        <div class="f-images__wrapper d-flex d-flex-wrap">
            <div class="f-images__box f-images__left">
                <div class="f-images__content f-images__one__wrapper">
                    <div class="f-images__content__inner f-images__one">
                        <img class="img-fluid" src="<?= $image_one['url'] ?>" alt="" data-scroll data-scroll-speed="-2">
                    </div>
                </div>
                <div class="f-images__content f-images__three__wrapper">
                    <div class="f-images__content__inner f-images__three">
                        <img class="img-fluid" src="<?= $image_three['url'] ?>" alt="" data-scroll data-scroll-speed="-3">
                    </div>
                <img class="f-images__decoration f-images__decoration--one" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration20.png'); ?>" alt="plate with the food">
                </div>
            </div>
            <div class="f-images__box f-images__right">
                <div class="f-images__content f-images__two__wrapper">
                    <div class="f-images__content__inner f-images__two">
                        <img class="img-fluid" src="<?= $image_two['url'] ?>" alt="" data-scroll  data-scroll-speed="3">
                    </div>
                    <img class="f-images__decoration f-images__decoration--two" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration21.png'); ?>" alt="plate with the food">
                </div>
            </div>
        </div>
    </div>
</div>