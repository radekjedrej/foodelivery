<section class="testimonial testimonial-bg" data-scroll-section data-scroll data-scroll-repeat data-scroll-call="testimonialbg" data-bg="#F4F6FF">

    <div class="testimonial__header section__header u-text-center">
        <h3 class="heading-section bold">Reviews</h3>
    </div>

    <!-- Swiper -->
    <div class="swiper-container testimonial__container wrapper-stretched">
        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <div class="testimonial__rate">
                    <?php include get_icons_directory('stars.svg') ?>
                </div>
                <div class="testimonial__copy">
                    <p class="testimonial__text heading-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus magna odio, at pulvinar nibh pretium sed. Suspendisse sit amet interdum nunc”</p>
                    <span class="testimonial__intro heading-into bold"><span class="testimonial__intro--line"></span> Arkadiusz Mirenko</span>
                </div>
            </div>

            <div class="swiper-slide">
                <div class="testimonial__rate">
                    <?php include get_icons_directory('stars.svg') ?>
                </div>
                <div class="testimonial__copy">
                    <p class="testimonial__text heading-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus magna odio, at pulvinar nibh pretium sed. Suspendisse sit amet interdum nunc”</p>
                    <span class="testimonial__intro heading-into bold"><span class="testimonial__intro--line"></span> Arkadiusz Mirenko</span>
                </div>
            </div>


        </div>

        <div class="testimonial__navigation__wrapper wrapper-main">
            <div class="cerchio testimonial__prev swiper-button-prev"><?php include get_icons_directory('right-arrow.svg') ?></div>
            <div class="cerchio testimonial__next swiper-button-next"><?php include get_icons_directory('right-arrow.svg') ?></div>
        </div>
    </div>
    
    <footer class="testimonial__trustpilot__wrapper d-flex d-flex-center text-reviews">
        <div class="testimonial__trustpilot d-flex d-flex-center">
            <div class="testimonial__trustpilot__logo">
                <?php include get_icons_directory('trustpilot-logo.svg') ?>
            </div>
            <img class="testimonial__trustpilot__stars" src="<?php echo get_theme_file_uri('/src/images/svg/trustpilot-filled.svg'); ?>" alt="review stars icon">
            <div>
                <p class="color-gray">Our customers say Excellent</p>
                <p class="bold">4.8 / 5 based on 2.021 reviews</p>
            </div>
        </div>
    </footer>

</section>