<?php

$pricing_plan = hc_get_pricing_list();
$count__tab = 0;
$count__content = 0;
$tabs_directions = ['left', 'center', 'right'];

?>

<section class="pricing <?= (!empty($args['class'])) ? $args['class'] : ''?> <?= (!empty($args['margin-top'])) ? 'mt-5' : ''?>" data-scroll-section>
    <div class="wrapper-main">
    
        <div class="pricing__header section__header u-text-center">
            <h3 class="heading-section"><?= (array_key_exists('text-title',$args)) ? $args['text-title'] : 'Plans and<br> pricing'; ?></h3>
        </div>

        <div class="pricing__content">

            <!-- Tab links -->
            <div class="pricing__buttons__wrapper">
                <div class="pricing__buttons left" aria-label="Pricing plan">

                <?php foreach ( $pricing_plan as $key => $plans ) :
                ?>
                    <a href="#" class="tablinks d-flex d-flex-center <?= $count__tab === 0 ? 'active' : ''; ?>" data-country="<?= $key ?>" data-direction="<?= $tabs_directions[$count__tab]; ?>" aria-selected="<?= $count__tab === 0 ? 'true' : 'false'; ?>" aria-controls="<?= $key ?>-plan"><p class="mb-0 regular color-text-gray3"><?= ucfirst($key) ?></p></a>

                <?php 
                    $count__tab++;
                endforeach;
                ?>
            
                </div>
            </div>

            <!-- Tab content -->
            <div class="pricing__options">
                <?php foreach ( $pricing_plan as $key => $plans ) : ?>


                    <div id="<?= $key ?>" class="tabcontent <?= $count__content === 0 ? 'active' : ''; ?>" role="tabpanel" id="<?= $key ?>-plan" aria-labelledby="<?= $key ?>">
                        <div class="pricing__list <?= $count__content === 0 ? 'pricing__list--active' : ''; ?> pricing__list__<?= $key ?> d-flex d-flex-wrap">

                            <?php foreach ( $plans as $key => $plan ) :
                            $popular = $plan['popular'];
                            $name = $plan['name'];
                            $per = $plan['per'];
                            $cost = $plan['cost'];
                            $cost_per = $plan['cost_per'];
                            $save = $plan['save'];
                            $url = $plan['url'];
                            ?>

                            <a href="<?= $url ?>" class="pricing__box u-text-center">
                                <div class="pricing__box__inner">
                                    <div class="pricing__price__wrapper">
                                        <div>
                                            <?php if($popular): ?>
                                                <div class="pricing__popular color-white heading-tiny">POPULAR</div>
                                            <?php endif; ?>
                                        </div>
                                        <p class="pricing__price color-black"><span class="pricing__price__amount bold heading-text line-reset"><?= $cost ?></span><br><span class="line-reset d-block color-gray"><?= $cost_per ?></span></p>
                                    </div>
                                    <div class="pricing__main">
                                        <p class="pricing__main__intro line-reset d-block color-gray"><?= $per ?></p>
                                        <div class="pricing__main__title bold heading-content"><?= $name ?></div>
                                    </div>
                                    <div class="pricing__choose">
                                        <button class="cerchio pricing__choose-button bold">Choose</button>
                                    </div>
                                </div>
                            </a>
                            <?php endforeach; ?>

                        </div>
                    </div>



                <?php 
                    $count__content++;
                endforeach;
                ?>

            </div>
        </div>
    </div>
</section>