<section class="faq" data-scroll-section>
    
    <div class="faq__header section__header u-text-center">
        <h2 class="heading-section mb-5">Faq</h2>
    </div>

    <div class="wrapper-main">
        <div class="faq__wrapper">
            <?php if( have_rows('faq_group') ): ?>    
                <?php while( have_rows('faq_group') ) : the_row();
                    
                $title = get_sub_field('title');    
                $text = get_sub_field('text');    
                    
                ?>
    
                    <div class="faq__row">
                        <div class="faq__border">
            
                            <div class="faq__question">
                                <h3 class="js-accordion-button faq__title d-flex">
                                    <p class="faq__text heading-text medium line-md"><?= $title ?></p>
                                    <span class="faq__icon d-flex">
                                        <svg width="8" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.77.36L.253 3.879a.352.352 0 10.496.496l2.92-2.917v13.931a.352.352 0 10.705 0V1.458L7.29 4.375a.352.352 0 10.496-.496L4.267.359a.352.352 0 00-.496 0z" fill="#090321"/></svg>
                                    </span>
                                </h3>
                            </div>
                    
                            <div class="js-answer faq__answer">
                                <div class="faq__description">
                                    <p class="color-gray heading-text line-md"><?= $text; ?></p>
                                </div>
                            </div>
            
                        </div>
                    </div>
    
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    
</section>