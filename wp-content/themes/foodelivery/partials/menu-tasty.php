<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";

?>

<section class="tasty-menu__wrapper tasty-bg wrapper-stretched" data-scroll-section data-scroll data-scroll-repeat data-scroll-call="tastybg" data-bg="#F4F6FF" data-scroll-offset="600">

    <header class="wrapper-main wrapper__header u-text-center">
        <div class="tasty-menu__heading">
            <h3 class="heading-section"><?= $title ?></h3>
        </div>
    </header>

    <div class="tasty-menu__container wrapper-main">
        <div class="swiper-wrapper our-menu__list">

            <?php if( have_rows('product_group') ): ?>    
                <?php while( have_rows('product_group') ) : the_row();
                    
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $info = get_sub_field('info'); 
                ?>

                <div class="tasty-menu__slide swiper-slide">
                    <div class="tasty-menu__content tasty-menu__content cd-popup-trigger">
                        <img class="tasty-menu__image" src="<?= $image['url'] ?>" alt="">
                        <h4 class="tasty-menu__title heading-card bold"><?= $title ?></h4>
                        <div class="tasty-menu__text color-gray"><?= $text ?></div>
                        <div class="tasty-menu__info"><?= $info ?></div>
                    </div>
                </div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>

        <div class="swiper-button-prev">
            <div class="tasty-menu__prev cerchio"><?php include get_icons_directory('right-arrow.svg') ?></div>
        </div>
        <div class="swiper-button-next">
            <div class="tasty-menu__next cerchio"><?php include get_icons_directory('right-arrow.svg') ?></div>
        </div>

    </div>

    <div class="wrapper-main tasty-menu__button__wrapper">
        <div class="tasty-menu__button">
            <a class="u-btn" href="#">See full menu</a>
        </div>
    </div>




    <div class="cd-popup" role="alert">
        <div class="cd-popup-container">
            <a href="#0" class="cd-popup-close img-replace"></a>
            <div class="cd-popup__content">
                <div class="cd-popup__header">
                    <img class="cd-popup__header__image" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration21.png'); ?>" alt="">
                    <h3 class="cd-popup__header__title heading-popup">Roasted turkey and sweet potato salad with oriental sauce</h3>
                </div>
                <div class="cd-popup__body">
                    <div class="cd-popup__body__left">
                        <h4 class="heading-footer cd-popup__body__title">Roasted turkey and sweet potato salad with oriental sauce</h4>
                        <div class="heading-text color-gray">
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                        </div>
                    </div>
                    <div class="cd-popup__body__right">
                        <div class="cd-popup__body__neutrients">
                            <h4 class="cd-popup__body__neutrients__title heading-footer">Nutrients</h4>
                            <ul class="cd-popup__body__neutrients__ul">
                                <li class="cd-popup__body__neutrients__li">
                                    <div class="cd-popup__body__neutrients__icon">
                                        <?php include get_icons_directory('calories.svg') ?>
                                    </div>
                                    <div class="cd-popup__body__neutrients__amount">400</div>
                                    <div class="cd-popup__body__neutrients__type">calories</div>
                                </li>
                                <li class="cd-popup__body__neutrients__li">
                                    <div class="cd-popup__body__neutrients__icon">
                                        <?php include get_icons_directory('calories.svg') ?>
                                    </div>
                                    <div class="cd-popup__body__neutrients__amount">81g</div>
                                    <div class="cd-popup__body__neutrients__type">carbs</div>
                                </li>
                                <li class="cd-popup__body__neutrients__li">
                                    <div class="cd-popup__body__neutrients__icon">
                                        <?php include get_icons_directory('calories.svg') ?>
                                    </div>
                                    <div class="cd-popup__body__neutrients__amount">15g</div>
                                    <div class="cd-popup__body__neutrients__type">protein</div>
                                </li>
                                <li class="cd-popup__body__neutrients__li">
                                    <div class="cd-popup__body__neutrients__icon">
                                        <?php include get_icons_directory('calories.svg') ?>
                                    </div>
                                    <div class="cd-popup__body__neutrients__amount">38.8g</div>
                                    <div class="cd-popup__body__neutrients__type">fat</div>
                                </li>
                            </ul>
                        </div>
                        <div class="cd-popup__body__ingredients">
                            <h4 class="cd-popup__body__ingredients__title heading-footer">Indgredients</h4>
                            <ul class="cd-popup__body__ingredients__ul">
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                                <li class="cd-popup__body__ingredients__li">
                                    <img class="cd-popup__body__ingredients_img" src="<?php echo get_theme_file_uri('/src/images/food/pasta.png'); ?>" alt="">
                                    <div class="cd-popup__body__ingredients__name">Pasta</div>
                                </li>
                            </ul>
                        </div>                  
                    </div>
                </div>
            </div>
        </div> <!-- cd-popup-container -->
    </div> <!-- cd-popup -->

</section>
