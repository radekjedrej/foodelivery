<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$button = get_sub_field("button") ? get_sub_field("button") : "";

?>

<section class="u-text d-relative heading-bg" data-scroll-section data-scroll data-scroll-repeat data-scroll-call="headingbg" data-bg="#F4F6FF" data-scroll-section data-scroll-delay="2000" data-scroll-offset="400">
    <div class="u-text__header u-text-center">
        <h2 class="u-text__title heading-billboard"><?= $title ?></h2>
        <p class="u-text__copy line-md heading-text color-gray"><?= $text ?></p>
        <?php if($button): ?>
        <a class="u-btn u-btn--lg" href="<?= $button['url'] ?>" class="h-btn"><?= $button['title']  ?></a>
        <?php endif; ?>
    </div>

    <img class="u-text__decoration u-text__decoration-1" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration1.png'); ?>" alt="person icon" data-scroll>
    <img class="u-text__decoration u-text__decoration-2" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration2.png'); ?>" alt="person icon" data-scroll>
    <img class="u-text__decoration u-text__decoration-3" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration3.png'); ?>" alt="person icon" data-scroll>
    <img class="u-text__decoration u-text__decoration-4" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration4.png'); ?>" alt="person icon" data-scroll>
    <img class="u-text__decoration u-text__decoration-5" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration5.png'); ?>" alt="person icon" data-scroll>
    <img class="u-text__decoration u-text__decoration-6" src="<?php echo get_theme_file_uri('/src/images/decoration/decoration6.png'); ?>" alt="person icon" data-scroll>

</section>