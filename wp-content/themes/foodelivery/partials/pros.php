<section class="u-pros" data-scroll-section>
    <div class="wrapper-main">
        <div class="u-pros__inner d-flex d-flex-wrap">
            <div class="hover-this u-pros__first" data-video="/foodelivery/wp-content/themes/foodelivery/src/video/test3.mp4">
                <div class="hover-this__inner">
                    <h3 class="heading-pros"><span>Real Food</span></h3>
                </div>
            </div>
            <div class="hover-this u-pros__second" data-video="/foodelivery/wp-content/themes/foodelivery/src/video/test3.mp4">
                <div class="hover-this__inner">
                    <h3 class="heading-pros u-pros__second"><span>Real ingredients</span></h3>
                </div>
            </div>
            <div class="hover-this u-pros__third" data-video="/foodelivery/wp-content/themes/foodelivery/src/video/test.mp4">
                <div class="hover-this__inner">
                    <h3 class="heading-pros u-pros__third"><span>Really fast</span></h3>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="u-pros__video">
    <video class="u-pros__video__content" src="/foodelivery/wp-content/themes/foodelivery/src/video/test.mp4" autoplay muted loop></video>
</div>
