<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";

?>

<section class="m-text" data-scroll-section data-scroll data-scroll-repeat data-scroll-call="foodelivery-bg" data-bg="#FFFFFF" data-scroll-offset="200">
    <div class="wrapper-main">
        <div class="m-text__inner">
            <h3 class="m-text__title heading-text bold"><?= $title ?></h3>
            <p class="m-text__text heading-text color-gray"><?= $text ?></p>
        </div>
    </div>
</section>