<?php if( have_rows('icons_group') ): ?>
<section class="scroll-icons__wrapper" id="direction" data-scroll-section>
    <div data-scroll data-scroll-direction="horizontal" data-scroll-speed="8" data-scroll-target="#direction" data-scroll-delay="0.05">
        <div class="d-flex scroll-icons">

            <?php while( have_rows('icons_group') ) : the_row();
                $icon = get_sub_field('icon');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
            ?>
        
            <div class="scroll-icons__box" >
                <div class="scroll-icons__img"><img src="<?= $icon['url'] ?>" alt=""></div>
                <div class="scroll-icons__copy">
                    <h2 class="scroll-icons__title heading-section"><?= $title ?></h2>
                    <div class="scroll-icons__text heading-text color-gray"><?= $text ?></div>
                </div>
            </div>
        
            <?php endwhile; ?>

        </div>
    </div>
</section>
<?php endif; ?>
