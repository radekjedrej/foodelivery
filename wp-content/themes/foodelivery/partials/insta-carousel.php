<?php
$insta_link = get_sub_field("insta_link") ? get_sub_field("insta_link") : "";
?>

<section class="insta-carousel__wrapper" data-scroll-section data-scroll-delay="2000">
    <div class="insta-carousel swiper-container">
        <div class="swiper-wrapper d-flex">

            <?php $count = 1; ?>
            <?php if( have_rows('photos_group') ): ?>    
                <?php while( have_rows('photos_group') ) : the_row();
                    
                $image = get_sub_field('image');         
                ?>

                <div class="insta-carousel__box swiper-slide">
                    <div class="insta-carousel__curtain insta-carousel__curtain--<?= $count ?>" data-scroll></div>
                    <img class="insta-carousel__img img-fluid" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                </div>

                <?php $count++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            
        </div>
        
        <div class="insta-carousel__navigation__wrapper wrapper-main">
            <div class="cerchio insta-carousel__prev swiper-button-prev"><?php include get_icons_directory('right-arrow.svg') ?></div>
            <div class="cerchio insta-carousel__next swiper-button-next"><?php include get_icons_directory('right-arrow.svg') ?></div>
        </div>
    </div>
</section>
