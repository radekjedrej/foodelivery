<?php
/**
 * Template name: Foodelivery Homepage
 */

get_header(); ?>

	<main id="main" role="main">
		
		<?php get_template_part( 'partials/billboard' ); ?>



		<?php
		while ( have_posts() ) :
			the_post();
			the_content();
		endwhile; // End of the loop.
		?>
	</main><!-- #main -->
<?php
get_footer();