<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" data-scroll-container>

		<?php
		fuzion_layout();
		the_content();
		get_template_part( 'template-parts/footer' );

		?>

		<!-- <div class="cursor"></div> -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
