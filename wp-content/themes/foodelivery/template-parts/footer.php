<footer id="colophon" class="u-footer" data-scroll-section>
    <div class="wrapper-main">
        <div class="u-footer__welcome">
            <h3 class="u-footer__welcome__title heading-content color-white">10%<br> Subscribe new product</h3>
            <a class="u-footer__welcome__mail color-white" href="mailto:contact@foodelivery.com">contact@foodelivery.com</a>
            <div class="u-footer__welcome__line"></div>
            <p class="u-footer__welcome__text color-gray">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum</p>
            <a class="u-footer__welcome__subscribe cerchio d-flex d-flex-center bold" href="#">Subscribe</a>
        </div>
        <div class="u-footer__main">
            <div class="u-footer__logo">
                <?php include get_icons_directory('logo.svg') ?>
            </div>
            <div class="u-footer__list">
                <ul class="u-footer__ul color-white">
                    <li class="u-footer__li__title bold">Company</li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Plans & Menus</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">How it Works</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Help</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Get 50 PL Off</a></li>
                </ul>
            </div>
            <div class="u-footer__list">
                <ul class="u-footer__ul color-white">
                    <li class="u-footer__li__title bold">Company</li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Plans & Menus</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">How it Works</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Help</a></li>
                    <li class="u-footer__li"><a class="u-footer__cta" href="#">Get 50 PL Off</a></li>
                </ul>
            </div>
            <div class="u-footer__contact color-white">
                <div class="u-footer__phone heading-footer bold">+48 611 913 133</div>
                <div class="u-footer__mail heading-footer bold"><a href="mailto:contact@foodelivery.com">contact@foodelivery.com</a></div>
                <div class="u-footer__social">
                    <a class="u-footer__social__cta" href="#"><?php include get_icons_directory('instagram.svg') ?></a>
                    <a class="u-footer__social__cta" href="#"><?php include get_icons_directory('facebook.svg') ?></a>
                    <a class="u-footer__social__cta" href="#"><?php include get_icons_directory('linkedin.svg') ?></a>
                </div>
            </div>
        </div>
        <div class="footer__details">
            <div class="footer__rights color-gray">© FoodDelivery 2021 | All Rights Reserved</div>
            <div class="footer__cards">
                <ul class="footer__cards__ul d-flex">
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/visa.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/mc.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/stripe.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/american.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/google.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/apple.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/klarna.png'); ?>" alt="">
                    </li>
                    <li class="footer__cards__li">
                        <img src="<?php echo get_theme_file_uri('/src/images/cards/paypal.png'); ?>" alt="">
                    </li>
                </ul>
            </div>
            <div class="footer__privacy">
                <a class="footer__privacy__cta color-gray" href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->