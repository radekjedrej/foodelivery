<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */
?>

<div class="entry-content">
	<?php the_content(); ?>
</div><!-- .entry-content -->
