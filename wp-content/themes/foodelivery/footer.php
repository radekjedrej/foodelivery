<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div><!-- #content -->
</div><!-- #page -->

<script>
	const wrapper = document.querySelector('.woocommerce')
	wrapper.setAttribute('data-scroll-section', '')
	console.log(wrapper)
</script>

<?php wp_footer(); ?>

</body>
</html>

